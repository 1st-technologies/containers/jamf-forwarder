FROM python:alpine3.16

RUN python3 -m pip install --no-cache-dir --upgrade pip==22.3.1 \
  && python3 -m pip install --no-cache-dir requests==2.28.1 flask==2.2.2

RUN mkdir /scripts
COPY contents/* /scripts/

ENTRYPOINT ["python3","/scripts/jamf-forwarder.py"]