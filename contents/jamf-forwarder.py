import json
import requests
import argparse
import textwrap
from flask import Flask, jsonify, request

if __name__ == '__main__':
    # Get Arguments
    description = """
    Forward Jamf Webhooks to Teams
  """

    epilog = """
    example:
    python jamf-fowarder.py -u https://company.webhook.office.com/webhookb2/00000000-0000-0000-0000-00000000000000000000-0000-0000-0000-000000000000/IncomingWebhook/00000000000000000000000000000000/00000000-0000-0000-0000-000000000000
  """
    parser = argparse.ArgumentParser(
        description=textwrap.dedent(description),
        epilog=textwrap.dedent(epilog),
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    parser.add_argument(
        "-u",
        "--url",
        help="Specifies the Teams Webhook URL",
        required=False,
        dest='url',
        default='https://company.webhook.office.com/webhookb2/00000000-0000-0000-0000-00000000000000000000-0000-0000-0000-000000000000/IncomingWebhook/00000000000000000000000000000000/00000000-0000-0000-0000-000000000000'
    )

    parser.add_argument(
        "-p",
        "--port",
        help="Specifies the listening port",
        required=False,
        dest='port',
        default='8000'
    )

    parser.add_argument(
        "-l",
        "--listen",
        help="Specifies the listening IP",
        required=False,
        dest='listen',
        default='0.0.0.0'
    )

    parser.add_argument(
        "-k",
        "--key",
        help="Specifies the authentication key",
        required=False,
        dest='key',
        default='00000000-0000-0000-0000-000000000000'
    )

    args = parser.parse_args()

    # Create Teams Card Template
    template = """{
        "@type": "MessageCard",
        "@context": "http://schema.org/extensions",
        "themeColor": "490E4b",
        "summary": "",
        "sections": [{
            "activityTitle": "Jamf",
            "activitySubtitle": "",
            "facts": [],
            "markdown": "true"
        }]
    }"""

    # Define Web Receiver
    app = Flask(__name__)
    @app.route('/webhook', methods=['POST'])
    def webhook():
        headers = request.headers
        auth = headers.get("X-Api-Key")
        if auth == args.key:
            if request.method == 'POST':
                # Load results and Teams json template
                rawobj = request.json
                templateobj = json.loads(template)

                # Get Jamf Webhook event properties and append to Teams MessageCard "facts"
                templateobj["summary"] = rawobj["webhook"]["webhookEvent"]
                templateobj["sections"][0]["activitySubtitle"] = rawobj["webhook"]["webhookEvent"]

                for property in rawobj["event"]["computer"]:
                    templateobj["sections"][0]["facts"].append({"name": property, "value": rawobj["event"]["computer"][property]})

                # Send to Teams Webhook
                headers = {'Content-type': 'application/json'}
                requests.post(args.url, headers=headers, data=json.dumps(templateobj))
                result = jsonify({"message": "OK: Authorized"}), 200

        else:
            result = jsonify({"message": "ERROR: Unauthorized"}), 401

        return result

    # Run Flask Server
    app.run(host=args.listen, port=args.port)