import json
import requests
import textwrap

if __name__ == '__main__':
    # sample data
    raw = """{
    "webhook": {
        "id": 3,
        "name": "Test",
        "webhookEvent": "ComputerCheckIn",
        "eventTimestamp": 1643733523998
    },
    "event": {
        "trigger": "CLIENT_CHECKIN",
        "username": "xxxxxx",
        "computer": {
            "udid": "00000000-C51D-0000-0000-000000000000",
            "deviceName": "MYMACBOOK",
            "model": "MacBook Pro (13-inch, 2020, Four Thunderbolt 3 ports)",
            "macAddress": "00:00:00:00:00:00",
            "alternateMacAddress": "00:00:00:00:00:01",
            "serialNumber": "123456789012",
            "osVersion": "12.1.0",
            "osBuild": "21C52",
            "userDirectoryID": "",
            "username": "xxxxxx",
            "realName": "",
            "emailAddress": "",
            "phone": "",
            "position": "",
            "department": "",
            "building": "",
            "room": "",
            "ipAddress": "10.10.10.10",
            "reportedIpAddress": "192.168.0.10",
            "jssID": 1
            }
        }
    }"""

    headers = {'Content-type': 'application/json', 'Accept': 'text/plain', 'X-Api-Key': '00000000-0000-0000-0000-000000000000'}
    result = requests.post("http://127.0.0.1:8000/webhook", headers=headers,  data=raw)
    print(result)