# Overview

This project demonstrates a simple web service in a container that includes a Python Flask http server, wrapped in a Docker Image. This application receives incoming Jamf Webhooks, translates them to Teams JSON "Cards" and forwards to the specified Teams "Incoming Webhook" URL.

If you want to customize this project for your own use skip ahead to [How to use this image](#how-to-use-this-image) below. If you prefer to use this image, it can be pulled from docker hub using the `docker pull 1sttec/jamf-forwarder` command.

| NOTE: This utility currently only functions with "Computer..." type Jamf webhook events. |
| --- |

[[_TOC_]]

## Prerequisites

Readers of this document are assumed to have working knowledge of Linux and Windows Administration, and Git. If you are not familiar with the latter, see this [git primer](https://danielmiessler.com/study/git/) before proceeding.

* A Gitlab account or local instance
* A DockerHub account or a local docker registry

Both the development workstation and any Gitlab runners will require the following:

* [Docker](https://www.docker.com/pricing) - Container Image utilities and runtime.
* [Python](https://python.org) - Python language interpreter and runtime
* [Make](https://www.gnu.org/software/make/) - Development automation utility
* [Hadolint](https://github.com/hadolint/hadolint) - Dockerfile validator

The development workstation must have an advanced Integration Development Environment (IDE) application such as [Microsoft Visual Studio Code](https://code.visualstudio.com/).

## Python

The core of any web service is an HTTP server. For this we use Python and [Flask](https://flask.palletsprojects.com/en/2.3.x/). Flask is a Python module and framework for developing web applications.

We begin by loading the required modules using the `include` statement. **NOTE**: You may have to install the modules using the command `python -m pip install requests argparse flask`. The `json` and `textwrap` modules are typically included but you may have to install them as well.

Alternatively, you can install required modules using the included [requirements.txt](https://learnpython.com/blog/python-requirements-file/) file using the following command `python -m pip install -r requirements.txt`.

```python
import json
import requests
import argparse
import textwrap
from flask import Flask, jsonify, request
```

Next we configure command line help and argument parsing...

```python
if __name__ == '__main__':
    # Get Arguments
    description = """
    Forward Jamf Webhooks to Teams
  """

    epilog = """
    example:
    python jamf-fowarder.py -u https://company.webhook.office.com/webhookb2/00000000-0000-0000-0000-00000000000000000000-0000-0000-0000-000000000000/IncomingWebhook/00000000000000000000000000000000/00000000-0000-0000-0000-000000000000
  """
    parser = argparse.ArgumentParser(
        description=textwrap.dedent(description),
        epilog=textwrap.dedent(epilog),
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    parser.add_argument(
        "-u",
        "--url",
        help="Specifies the Teams Webhook URL",
        required=False,
        dest='url',
        default='https://company.webhook.office.com/webhookb2/00000000-0000-0000-0000-00000000000000000000-0000-0000-0000-000000000000/IncomingWebhook/00000000000000000000000000000000/00000000-0000-0000-0000-000000000000'
    )

    parser.add_argument(
        "-p",
        "--port",
        help="Specifies the listening port",
        required=False,
        dest='port',
        default='8000'
    )

    parser.add_argument(
        "-l",
        "--listen",
        help="Specifies the listening IP",
        required=False,
        dest='listen',
        default='0.0.0.0'
    )

    parser.add_argument(
        "-k",
        "--key",
        help="Specifies the authentication key",
        required=False,
        dest='key',
        default='00000000-0000-0000-0000-000000000000'
    )

    args = parser.parse_args()
```

Then we create a Teams "card" data template structure to modify later...

```python
    # Create Teams Card Template object
    template = """{
        "@type": "MessageCard",
        "@context": "http://schema.org/extensions",
        "themeColor": "490E4b",
        "summary": "",
        "sections": [{
            "activityTitle": "Jamf",
            "activitySubtitle": "",
            "facts": [],
            "markdown": "true"
        }]
    }"""
```

Next we define a web service end point at "/webhook"...

```python
    # Define Web Receiver
    app = Flask(__name__)
    @app.route('/webhook', methods=['POST'])
    def webhook():
```

Then we get the header and check if contains the correct api key...

```python
        headers = request.headers
        auth = headers.get("X-Api-Key")
        if auth == args.key:
```

If a "POST" is received, extract the JSON values and update the Teams card template object values...

```python
            if request.method == 'POST':
                # Load results and Teams json template
                rawobj = request.json
                templateobj = json.loads(template)

                # Get Jamf Webhook event properties and append to Teams MessageCard "facts"
                templateobj["summary"] = rawobj["webhook"]["webhookEvent"]
                templateobj["sections"][0]["activitySubtitle"] = rawobj["webhook"]["webhookEvent"]

                for property in rawobj["event"]["computer"]:
                    templateobj["sections"][0]["facts"].append({"name": property, "value": rawobj["event"]["computer"][property]})
```

Then post the Teams card JSON content to the Teams incoming webhook URL.

```python

                # Send to Teams Webhook
                headers = {'Content-type': 'application/json'}
                requests.post(args.url, headers=headers, data=json.dumps(templateobj))
                result = jsonify({"message": "OK: Authorized"}), 200

        else:
            result = jsonify({"message": "ERROR: Unauthorized"}), 401

        return result
```

Lastly, execute the web server.

```python
    # Run Flask Server
    app.run(host=args.listen, port=args.port)
```

### Test

To test the server, first execute the web service script above on the local system and run the following test script in another terminal to see if the mock alert is sent to the designated Teams incoming web service hook.

```python
import json
import requests
import textwrap

if __name__ == '__main__':
    # sample data
    raw = """{
    "webhook": {
        "id": 3,
        "name": "Test",
        "webhookEvent": "ComputerCheckIn",
        "eventTimestamp": 1643733523998
    },
    "event": {
        "trigger": "CLIENT_CHECKIN",
        "username": "xxxxxx",
        "computer": {
            "udid": "00000000-C51D-0000-0000-000000000000",
            "deviceName": "MYMACBOOK",
            "model": "MacBook Pro (13-inch, 2020, Four Thunderbolt 3 ports)",
            "macAddress": "00:00:00:00:00:00",
            "alternateMacAddress": "00:00:00:00:00:01",
            "serialNumber": "123456789012",
            "osVersion": "12.1.0",
            "osBuild": "21C52",
            "userDirectoryID": "",
            "username": "xxxxxx",
            "realName": "",
            "emailAddress": "",
            "phone": "",
            "position": "",
            "department": "",
            "building": "",
            "room": "",
            "ipAddress": "10.10.10.10",
            "reportedIpAddress": "192.168.0.10",
            "jssID": 1
            }
        }
    }"""

    headers = {'Content-type': 'application/json', 'Accept': 'text/plain', 'X-Api-Key': '00000000-0000-0000-0000-000000000000'}
    result = requests.post("http://127.0.0.1:8000/webhook", headers=headers,  data=raw)
    print(result)
```

## Dockerfile

Next, we containerize the python script into an image for ease of execution. Containers are defined by a "Dockerfile" script.

The first line of the Dockerfile tells the docker build command to use python:alpine3.16 as the base image. This image already contains the correct version of python installed into [Alpine Linux](https://www.alpinelinux.org/), a very slim linux distribution well suited for containerization.

```Dockerfile
FROM python:alpine3.16
```

Next we use the **RUN** command to install the necessary python modules into the container. Every **RUN** command adds a [layer](https://docs.docker.com/build/guide/layers/), potentially increasing the size of the resulting image. It's good practice to combine multiple commands into one line using the bash "&&" directive.

Also, any installs, whether via yum, apt or pip should specify known good versions. e.g. `pip install requests==2.28.1` **NOT** `pip install requests`.

```Dockerfile
RUN python3 -m pip install --no-cache-dir --upgrade pip==22.3.1 \
  && python3 -m pip install --no-cache-dir requests==2.28.1 flask==2.2.2
```

Then we need to create a directory within the image to house the scripts...

```Dockerfile
RUN mkdir /scripts
```

and then we copy all the files in the local "contents" directory into the "/scripts" directory within the image.

```Dockerfile
COPY contents/* /scripts/
```

Last we tell the image to always run the script whenever it starts up using the **ENTRYPOINT** command.

```Dockerfile
ENTRYPOINT ["python3","/scripts/jamf-forwarder.py"]
```

Place the Dockerfile in the root of your source directory and place the python script in a "contents" directory under the source directory like so:

```text
━━┳━━ Dockerfile
  ┃ 
  ┗━━ contents ━━━━ jamf-forwarder.py
```

Ensure that you are in the root of the source directory then in the terminal, type `docker build -t myImage .` to build the image. To execute the image locally, type `docker run -ti --rm -p 8000:8000 myImage` where the "-p" parameter instructs Docker to map the local computer's TCP port 8000 to the containers port of the same number.

In a new terminal execute the test script while monitoring the container terminal's output.

You should see 200 response in both the test and docker run terminals.

```cmd
c:\source\containers\jamf-forwarder>python test.py
<Response [200]>
```

```cmd
c:\source\containers\jamf-forwarder>docker run -ti --rm -p 8000:8000 myImage
 * Serving Flask app 'jamf-forwarder'
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on all addresses (0.0.0.0)
 * Running on http://127.0.0.1:8000
 * Running on http://172.17.0.2:8000
Press CTRL+C to quit
172.17.0.1 - - [18/Sep/2023 12:07:29] "POST /webhook HTTP/1.1" 200 -
```

| A Note on Production Use |
| --- |
| You'll notice a warning message that Flask is not intended as a production (i.e. user-facing) web server. See the [Production Section](#production) for more information. |

To delete the temporary image type `docker rmi myImage`.

## Make

This project uses the make command and a [Makefile](https://www.gnu.org/software/make/manual/make.html). This is completely optional and all the commands specified in the Makefile can just as easily be transposed directly into the .gitlab-ci.yml file. The advantage of using the **make** command and a **Makefile** "script" is that you can more easily build and test on the local system before your initial commit to a code repository. ...nor is the effort lost given you can call the same test "make" targets in your production pipeline.

If you plan to fork this project or host it in your own repository, modify the default variables in the Makefile to reflect your repo host and namespace.

Make for windows can be downloaded [here](https://gnuwin32.sourceforge.net/packages/make.htm). To install make on "debian" linux distros (e.g. Debian, Ubuntu, alpine, etc) execute `sudo apt install make`. For Redhat derived distros (e.g. Redhat, Centos, RockyLinux) execute `sudo dnf groupinstall "Development Tools"`. For macOS, open Terminal and execute `xcode-select –install`.

### Anatomy of a Makefile for Container Development

Makefile code blocks are separated into "Targets" or "Goals". Each target can be called as the first parameter of the make command. For instance, executing `make test` will run all the code under the `test:` section of the Makefile.

Typically, the target name would reflect actual files generated by the code within the target, and the block will not run if that file already exists. For instance a target called `helloWorld:` with a code block `gcc HelloWorld.c -o helloWorld` will NOT run (assuming the previous compilation was successful) if a file called `helloWorld` already exists in the current directory.

However, since we are using the Makefile as a wrapper script, we don't want the make command to test for the existence of files. So we must first set all targets to "PHONY".

```makefile
.PHONY: publish build push clean run check stop lint test
```

Secondly, since we don't want accidental executions, we set the default target (or goal) to the "help" target which displays usage instructions.

```makefile
.DEFAULT_GOAL := help
```

Next we set default values for all variables including; the Docker registry name (or hostname if you are hosting your own Image registry), the image name and version, the Teams Incoming Web Hook URL, the listening IP (0.0.0.0 for all interfaces), the listening TCP port and an API key that will be checked for on all incoming HTTP Posts.

```makefile
REGISTRY:=1sttec
IMAGE_NAME:=jamf-forwarder
IMAGE_VERSION:=0.1.1
TEAMS_URL:=https://company.webhook.office.com/webhookb2/00000000-0000-0000-0000-00000000000000000000-0000-0000-0000-000000000000/IncomingWebhook/00000000000000000000000000000000/00000000-0000-0000-0000-000000000000
LISTENER:=0.0.0.0
PORT:=8000
KEY:=00000000-0000-0000-0000-000000000000
```

Then we configure a "help" target which will display all targets and any target usage documentation. Target usage is defined as any text after the double hash (##) after the target name.

```makefile
## make help that works in either Linux or Windows
help: ## Show this help
ifeq ($(OS),Windows_NT)
	@echo Usage:
	@echo   make ^<target^>
	@cmd /c echo.
	@echo Targets:
	@for /f "tokens=1,3* delims=:#" %%a in ('@find " ##" ^< $(MAKEFILE_LIST) ^| find /v "find"') do echo   %%a:%%b
	@cmd /c echo.
else
	@echo "Usage:"
	@echo "  make <target>"
	@echo ""
	@echo "Targets:"
	@sed -ne '/@sed/!s/## //p' $(MAKEFILE_LIST) | sed -e 's/^/  /'
	@echo ""
endif
```

Next we define a "build" target that executes the docker build command with the appropriate tags as specified by the variables listed above.

```makefile
build: ## Build image
	@docker build --no-cache -t ${REGISTRY}/${IMAGE_NAME}:latest -t ${REGISTRY}/${IMAGE_NAME}:${IMAGE_VERSION} .
```

Then we create a "push" target that is responsible for logging into DockerHub (or your local Docker registry), pushing the image and logging out again.

```makefile
push: ## Push image to internal repo
	@docker login --username ${USERID} --password ${PASSWORD}
	@docker push ${REGISTRY}/${IMAGE_NAME}:${IMAGE_VERSION}
	@docker push ${REGISTRY}/${IMAGE_NAME}:latest
	@docker logout
```

Next we define a "clean" target to remove the local images.

```makefile
clean: ## Delete image
	@docker rmi ${REGISTRY}/${IMAGE_NAME}:${IMAGE_VERSION}
	@docker rmi ${REGISTRY}/${IMAGE_NAME}:latest
```

For local testing, we can define a "run" target that will execute the image in the background.

```makefile
run: ## Run service
	@docker run --detach --rm --name ${IMAGE_NAME}:${IMAGE_VERSION} -p 8000:8000 ${REGISTRY}/${IMAGE_NAME} -u ${TEAMS_URL} -l ${LISTENER} -p ${PORT} --key "${KEY}"
ifeq ($(OS),Windows_NT)
	@timeout 3 > NUL
else
	@sleep 3
endif
```

As part of the testing procedure, an additional "check" target runs the test.py script against the localhost port 8000 and return the status.

```makefile
check: ## Test a running container
	@python ./test.py
```

Then we create a "stop" target to stop any instances of the Image.

```makefile
stop: ## Stop the container
ifeq ($(OS),Windows_NT)
	@docker stop ${IMAGE_NAME} > NUL
else
	@docker stop ${IMAGE_NAME} > /dev/null
endif
```

For the CI/CD "test" stage, we define a "lint" target to automate the checking of Dockerfile syntax. "Lint" is the computer science term for a static code analysis used to flag programming errors, bugs, stylistic errors and suspicious constructs.

```makefile
lint: ## check Dockerfile
	@hadolint --failure-threshold error ./Dockerfile
```

Makefile targets can call other target as "dependencies". The "publish" target has no code but rather calls the "build", "push" and "clean" targets in turn. In this manner the CI code only needs to call "make publish" to perform a complete update of the image in the designated Docker registry.

```makefile
publish: build push clean ## Run the build, push and clean targets
```

Lastly, we build a "test" target that calls the "run", "check" and "stop" targets to allow the developer to quickly test an image.

```makefile
test: run check stop ## Run a test
```

Using a make file reduces the amount of typing required during development as new edits can be tested with two commands; `make build` and `make test`. If the test is successful the engineer then only needs to execute `make publish USERID=((USERID)) PASSWORD=((PASSWORD))` to publish changes.

For a complete list of all the make targets and their usage see the [Make targets](#make-targets) section below.

## CI/CD

Continuous Integration/Continuous Delivery (CI/CD) is the practice of streamlining the testing, compilation, merging and deployment of source code to end-points. GitLab combines Version Control and CI/CI in one service. In this project, CI techniques are employed to automate several of the manual functions involved in building and deploying of images to DockerHub.

In Gitlab, CI is mediated by the .gitlab-ci.yml file. A simple .gitlab-ci.yml might look like the following.

```yml
stages:
  - doTheThing

run_doTheThing:
  stage: doTheThing
  tags:
    - im-a-tag  # <-- WHERE to run
    # tags are assigned to gitlab runners when installed,
    # setting a tag in a stage tells gitlab to only run the
    # stage on runners with matching tags; tags can be used to
    # designate OS, executor type and/or software dependencies.
  script: | # <-- WHAT to run
    # Configure Environment
    Do-TheThing

  only: # <-- WHEN to run (old way); this stage will only run on
  # certain conditions such a when merging to master branch
  # (i.e. ready for production)
    - master
 --OR--
  rules: # <-- WHEN to run (new way); rules are way more flexible.
    - if: '$CI_COMMIT_REF_NAME == "master"'
```

Gitlab does not do any of the execution but rather employs "runners" that you must supply and configure to do the actual work.

## How to configure a Gitlab Runner

This project employs a [Gitlab Runner](https://docs.gitlab.com/runner/) with the shell "executor".

1. If you have not already done so, create a Gitlab account and fork or download then push this repo to a new project.
1. navigate to **Settings** > **CI/CD** and note the url and registration token.
    ![Fig. 1](images/1.png)
1. Download the [Gitlab Runner for Windows x64](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe), copy it to C:\Gitlab-Runner and rename the file to "gitlab-runner.exe"
1. Open a command prompt as admin, CD to C:\gitlab-runner and enter the following commands:
   * `gitlab-runner.exe install`
   * `git-lab-runner.exe start`
   * `gitlab-runner.exe register`
1. When prompted, specify the URL noted above.:

   ```txt
   Enter the GitLab instance URL (for example, https://gitlab.com/):
   https://gitlab.com/
   ```

1. Specify the registration token noted above:

   ```txt
   Enter the registration token:
   XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
   ```

1. Type `Worker Node` as a description.:

   ```txt
   Enter a description for the runner:
   [myhost]: Worker Node
   ```

1. Specify tags `training`, substituting "env_**dev*" for your desired operational environment e.g. "dev", "uat", "prod", "test", etc...:

   ```txt
   Enter tags for the runner (comma-separated):
   training
   ```

   **NOTE**: Tags are freeform labels you attach to your runners and ýou typically won't see this many to designate runner dependencies. See [The Docker executor](https://docs.gitlab.com/runner/executors/docker.html) for more information.
1. Specify a maintenance note for the runner. This could include the number of CPU cores, location or environment, e.g. "dev", "prod", uat", etc...

   ```txt
   Enter optional maintenance note for the runner:
   test
   ```

1. Specify `shell` for the "executor".:

   ```txt
   Enter an executor: parallels, shell, ssh, virtualbox, docker+machine, docker-ssh+machine, docker, docker-ssh, instance, kubernetes, custom, docker-windows:
   shell
   ```

1. Ensure the "tags" section of all stages in the [.gitlab-ci.yml](.gitlab-ci.yml) file to reflect the tags specified above.
1. Type `gitlab-runner.exe stop`
1. Change the `[[runners]]` > `shell` value in c:\Gitlab-Runner\config.toml from "pwsh" to "powershell".

    ```toml
    ...
    [[runners]]
        name = "myRunner"
        url = "https://gitlab.com/"
        id = 00000000
        token = "xxxxxxxxxxxxxxxxxxxx"
        token_obtained_at = 1900-01-00T00:00:00Z
        token_expires_at = 0001-01-01T00:00:00Z
        executor = "shell"
        shell = "powershell"
    ...
    ```

1. Type `gitlab-runner.exe start`

A Gitlab pipeline can consist of as many stages as needed. Furthermore,each stage can have unique rules. For instance, you can run testing stages for every commit but only run deployment stages when committing to the master branch.

The [.gitlab-ci.yml](.gitlab-ci.yml) file in this project has two stages; "test" and "publish". The "test" stage tests for syntax errors using hadolint, this prevents accidental deployment of a malformed Dockerfile. The "publish" stage builds and uploads the image to the Docker image repository specified.

Once you have a runner configured you can construct the pipeline in the gitlab-ci.yml file.

First define the stages you will need. All pipeline should employ a "test" stage to check for common syntax errors.

```yml
stages:
  - test
  - publish
```

Each job stage must designate the **stage** to run, the **tags** of the runner on which to run, the actual command to run and the rules on **when** to run.

The _only ... pushes_ parameter tells gitlab to run the test stage on every push regardless of branch.

```yml
run-test:
  stage: test
  tags:
    - training
  script:
    - make lint
  only:
    - pushes
```

The publish stage runs the **make publish...** command with the appropriate variables and only executes when the code is committed to the master branch, i.e. it is ready for production.

```yml
run-publish:
  stage: publish
  tags:
    - training
  script:
    - make publish USERID=${CI_DOCKER_USERID} PASSWORD=${CI_DOCKER_TOKEN}
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

| A Note on Gitlab Runner "Executors" |
| --- |
| Larger enterprises typically do not use the "shell" executor as this requires an additional layer of management to ensure that the runners themselves have all the required software, somewhat defeating the purpose of using containers in the first place. Instead, Gitlab runners tend to be configured with **Docker** executors. A Gitlab runner using the "Docker" executor will run all build stages in a pre-configured docker image that is built explicitly as a worker node with all required software and libraries. See [Docker executor](https://docs.gitlab.com/runner/executors/docker.html) docs for more information. |

## How to Use this Image

### Build

1. Build the docker image by running the `make build` command as described [below](#make-targets). Alternatively, you can use the `make publish` command to build and push the image to a repo of your choice. You can publish the container to your own repo on [dockerhub](https://hub.docker.com/) see the [Docker Hub Quickstart](https://docs.docker.com/docker-hub/) guide and modify the [Makefile](Makefile) default variables as needed.
2. Create an Incoming Webhook in the Teams channel where you want alerts to be delivered per the [Create Incoming Webhooks](https://learn.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook) guide.
3. Execute the container with the appropriate arguments as outlined in the ["Make" commands](#make-targets) section below, setting the TEAMS_URL variable to the Teams Webhook URL generated in step 1.
4. Follow the process in the [Jamf Pro Administrators Guide](https://docs.jamf.com/10.31.0/jamf-pro/administrator-guide/Webhooks.html) to configure your desired outgoing webbooks setting options as defined below:
    * **Webhook URL**: The URL to the resulting endpoint of your running jamf-forwarder container.
    * **Authentication Type**: Header Authentication
    * **Header Authentication**:

      ```json
      {
        "Authorization":"X-Api-Key",
        "Token":"[API KEY]"
      }
      ```

      Where "[API KEY]" is the key defied when executing the container. See the [Make "targets"](#make-targets) section below.
    * **Connection Timeout**: 5
    * **Read Timeout**: 3
    * **Content Type**: JSON
    * **Webhook Event**: Select the desired event you want forwarded as an alert in Microsoft Teams.

If your Jamf instance can reach your development workstation, you will now be able to test by running a local instance of the container.

```mermaid
%%{ init: { 'flowchart': { 'curve': 'basis' } } }%%
flowchart LR
  %% Define Nodes
  forwarder("Jamf<br />Forwarder")
  jamf(Jamf)
  teams(Microsoft<br />Teams)
  
  %% Define Links
  jamf -- Jamf<br />Webhook --> forwarder
  forwarder -- Teams<br />Card --> teams

  %% Define Styles
  style forwarder fill:#fcee91,color:#000,stroke:#333,stroke-width:2px
  style jamf fill:#999,color:#fff,stroke:#333,stroke-width:2px
  style teams fill:#4d56c3,color:#fff,stroke:#333,stroke-width:2px
```

### Production

For production, you will need to host the container where it will be reachable by your Jamf instance, whether on-prem or in the cloud.

Specific methods for hosting a container is outside of the scope of this document. Note that the container runs a Flask server which is [not considered a production server](https://flask.palletsprojects.com/en/2.2.x/deploying/) and so should be placed behind a reverse proxy or load balancer.

```mermaid
%%{ init: { 'flowchart': { 'curve': 'basis' } } }%%
flowchart LR
  %% Define Nodes
  forwarder1("Jamf<br />Forwarder")
  forwarder2("Jamf<br />Forwarder")
  lb(Reverse Proxy/<br />SSL)
  jamf(Jamf)
  teams(Microsoft<br />Teams)
  
  %% Define Links
  jamf -- Jamf<br />Webhook --> lb
  lb ---> forwarder1 & forwarder2
  forwarder1 & forwarder2 -- Teams<br />Card --> teams

  %% Define Styles
  classDef forwarder fill:#fcee91,color:#000,stroke:#333,stroke-width:2px
  class forwarder1,forwarder2 forwarder
  style jamf fill:#999,color:#fff,stroke:#333,stroke-width:2px
  style teams fill:#4d56c3,color:#fff,stroke:#333,stroke-width:2px
  ```

This image can be hosted and scheduled locally in [Kubernetes](https://kubernetes.io/), [Nomad](https://www.nomadproject.io/) or [Docker Swam](https://docs.docker.com/engine/swarm/) or in a cloud solution such as [Azure Container Instances](https://azure.microsoft.com/en-us/products/container-instances/), [AWS Fargate](https://aws.amazon.com/fargate/) or [Google Cloud Run](https://cloud.google.com/run).

### Make targets

* **make help** - Display help
  * *Example*
    * Display usage information.
      `make help`
* **make build** - Build the image
  * *Variables*
    * REPOSITORY=[Docker repo and namespace]
    * IMAGE_NAME=[Docker image name]
    * IMAGE_VERSION=[Docker image version]
  * *Example*
    * Build the image and tag with default image name and version, also tag as "latest"
    `make build`
    * Build the image and tag with a custom repo, image name and version, also tag as "latest"
    `make build REGISTRY=myinternalrepo.domain.com/myinternalnamespace IMAGE_NAME=myjamf_forwarder IMAGE_VERSION=0.1`
* **make push** - Push the image to a docker repo
  * *Variables*
    * REPOSITORY=[Docker repo and namespace]
    * IMAGE_NAME=[Docker image name]
    * IMAGE_VERSION=[Docker image version]
    * USERID=[Docker repo user ID]
    * PASSWORD=[Docker repo password]
  * *Example*
    * Push to the default registry defined in the Makefile.
      `make push USERID=myrepoacct PASSWORD=MyPassword`
    * Push to image myregistry/mynamespace/myjamf_forwarder:1.0 and myjamf_forwarder:latest
    `make push REGISTRY=myinternalrepo.domain.com/myinternalnamespace IMAGE_NAME=myjamf_forwarder USERID=myrepoacct IMAGE_VERSION=1.0 PASSWORD=mypassword`
* **make clean** - Delete the image
  * *Example*
    * Delete the image
      `make clean`
* **make run** - Run the web service
  * *Variables*
    * TEAMS_URL=[Teams Webhook URL]
    * LISTENER=[Listening IP]
    * PORT=[Listening Port]
    * KEY=[Authentication Key]
  * *Example*
    * Listen for Jamf webhooks on port 8000. Reformat and forward json "card" to the default Teams Incoming Webhook.
    `make run TEAMS_URL=https://company.webhook.office.com/webhookb2/00000000-0000-0000-0000-00000000000000000000-0000-0000-0000-000000000000/IncomingWebhook/00000000000000000000000000000000/00000000-0000-0000-0000-000000000000 KEY=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX`
    * Listen for Jamf webhooks on port 8000 on all IP addresses (default). Reformat and forward json "card" to the specified Teams Incoming Webhook.
    `make run TEAMS_URL=https://company.webhook.office.com/webhookb2/00000000-0000-0000-0000-00000000000000000000-0000-0000-0000-000000000000/IncomingWebhook/00000000000000000000000000000000/00000000-0000-0000-0000-000000000000 KEY=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX`
* **make publish** - Build and push the image, clean when done
  * *Variables*
    * REPOSITORY=[Docker repo and namespace]
    * IMAGE_NAME=[Docker image name]
    * IMAGE_VERSION=[Docker image version]
    * USERID=[Docker repo user ID]
    * PASSWORD=[Docker repo password]
  * *Example*
    * Push to the default registry defined in the Makefile.
      `make publish USERID=myrepoacct PASSWORD=MyPassword`
    * Push to image myregistry/mynamespace/myjamf_forwarder:1.0 and myjamf_forwarder:latest
    `make publish REGISTRY=myregistry/mynamespace IMAGE_NAME=myjamf_forwarder USERID=myrepoacct IMAGE_VERSION=1.0 PASSWORD=mypassword`
* **make check** - run the test script and show http response
  * *Variables*
    * TEAMS_URL=[Teams Webhook URL]
    * LISTENER=[Listening IP]
    * PORT=[Listening Port]
    * KEY=[Authentication Key]
  * *Examples*
    * Connect to a running container on port 127.0.0.1:8080 and return HTTP status code
      `make check`
* **make stop** - stop a running container
  * *Example*
    * Stop a running container
      `make stop`
* **make test** - run, check and stop a container
  * *Variables*
    * TEAMS_URL=[Teams Webhook URL]
    * LISTENER=[Listening IP]
    * PORT=[Listening Port]
    * KEY=[Authentication Key]
  * *Examples*
    * Run the container, test the flask server and then stop the container
      `make test`
    * Run the container, test the flask server, attempt to send a test card to a Teams webhook and then stop the container
      `make test TEAMS_URL=https://company.webhook.office.com/webhookb2/00000000-0000-0000-0000-00000000000000000000-0000-0000-0000-000000000000/IncomingWebhook/00000000000000000000000000000000/00000000-0000-0000-0000-000000000000 KEY=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX`

## Index

* **[contents/](contents)** - The contents of this directory get copied into the /scripts directory of the resulting Docker image.
* **[.gitlab-ci.yml](.gitlab-ci.yml)** - Defines the gitlab CI/CD workflow to automatically build and publish this container on any merge to the master branch.
* **[Dockerfile](Dockerfile)** - Defined the build process for the image.
* **[jamf-forwarder.code-workspace](jamf-forwarder.code-workspace)** - The Microsoft VisualStudio Code workspace file for this project.
* **[Makefile](Makefile)** - Defines and automates various stages of build and testing for rapid development.
* **[README.md](README.md)** - This documentation
* **[requirements.txt](requirements.txt)** - A list of the required python modules.
* **[test.py](test.py)** - A test script that sends a mock Jamf alert to the Jamf Forwarder web service.

## License

View [license information](http://www.1st-technologies.com/license) for the software contained in this project.
